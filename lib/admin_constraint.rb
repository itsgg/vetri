class AdminConstraint
  def matches?(request)
    return false unless request.session[:current_user_id]
    user = User.find request.session[:current_user_id]
    user && user.admin?
  end
end
