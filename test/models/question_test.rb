# == Schema Information
#
# Table name: questions
#
#  id               :integer          not null, primary key
#  content          :text(65535)      not null
#  answer_id        :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  explanation_uuid :string(255)
#

require 'test_helper'

class QuestionTest < ActiveSupport::TestCase

  test 'validate presence' do
    question = Question.new
    assert !question.save
    assert question.errors[:content].include?("can't be blank")
  end

end
