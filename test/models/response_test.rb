# == Schema Information
#
# Table name: responses
#
#  id          :integer          not null, primary key
#  attempt_id  :integer          not null
#  question_id :integer          not null
#  answer_id   :integer          not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'test_helper'

class ResponseTest < ActiveSupport::TestCase

  test 'validate presence' do
    response = Response.new
    assert !response.save
    assert response.errors[:attempt].include?("can't be blank")
    assert response.errors[:question].include?("can't be blank")
    assert response.errors[:answer].include?("can't be blank")
  end

end
