# == Schema Information
#
# Table name: packages
#
#  id               :integer          not null, primary key
#  name             :string(255)      not null
#  description      :text(65535)
#  rating           :float(24)        default(0.0)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  profile          :string(255)
#  published        :boolean          default(FALSE)
#  rating_count     :integer          default(0)
#  rating_value     :integer          default(0)
#  has_test         :boolean          default(TRUE), not null
#  has_announcement :boolean          default(TRUE), not null
#  has_discussion   :boolean          default(TRUE), not null
#  has_lesson       :boolean          default(TRUE), not null
#  has_live_class   :boolean          default(TRUE), not null
#  cost             :decimal(10, )    default(0), not null
#  subtitle         :string(255)
#

require 'test_helper'

class PackageTest < ActiveSupport::TestCase
  setup do
    @package = packages(:neet)
  end

  test 'validate presence' do
    package = Package.new
    assert !package.save
    assert package.errors[:name].include?("can't be blank")
  end

  test 'relationship' do
    packages(:jipmer).users.include?(users(:akshi))
    packages(:iit).users.include?(users(:akshi))
    packages(:iit).users.include?(users(:gg))
    packages(:neet).users.include?(users(:gg))
  end

  test 'rate!' do
    @package.rate!(5)
    assert_equal 5, @package.rating
  end
end
