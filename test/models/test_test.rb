# == Schema Information
#
# Table name: tests
#
#  id                 :integer          not null, primary key
#  title              :string(255)      not null
#  description        :text(65535)      not null
#  package_id         :integer          not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  time_limit_in_mins :integer
#  negative_marks     :integer          default(0)
#  positive_marks     :integer          default(1)
#  dynamic            :boolean          default(FALSE)
#  questions_count    :integer          default(10)
#  published          :boolean          default(FALSE)
#

require 'test_helper'

class TestTest < ActiveSupport::TestCase
  setup do
    @user = users(:gg)
    @test = tests(:one)
    @attempt = attempts(:one)
  end

  test 'validate presence' do
    new_test = Test.new
    assert !new_test.save
    assert new_test.errors[:title].include?("can't be blank")
    assert new_test.errors[:package].include?("can't be blank")
  end

  test 'attempt' do
    assert_equal @attempt, @test.attempt(@user.id)
  end

  test 'complete?' do
    assert @test.complete?(@user.id)
  end

  test 'started_at' do
    assert_equal @attempt.created_at, @test.started_at(@user.id)
  end

end
