# == Schema Information
#
# Table name: answers
#
#  id          :integer          not null, primary key
#  content     :text(65535)      not null
#  question_id :integer          not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'test_helper'

class AnswerTest < ActiveSupport::TestCase

  test 'validations' do
    answer = Answer.new
    assert !answer.save
    assert answer.errors[:content].include?("can't be blank")
  end

  test 'selected?' do
    assert answers(:one_two).selected?(attempts(:one), questions(:one))
    assert !answers(:one_one).selected?(attempts(:one), questions(:one))
  end

end
