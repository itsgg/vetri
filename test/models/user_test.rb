# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  name            :string(255)      not null
#  email           :string(255)      not null
#  password_digest :string(255)      not null
#  admin           :boolean          default(FALSE)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  avatar          :string(255)
#  mobile          :string(255)
#

require 'test_helper'

class UserTest < ActiveSupport::TestCase

  test 'validate presence' do
    user = User.new
    assert !user.save
    assert user.errors[:name].include?("can't be blank")
    assert user.errors[:email].include?("can't be blank")
  end

  test 'validate uniqueness' do
    user = User.new email: users(:gg).email
    assert !user.save
    assert user.errors[:email].include?('has already been taken')
  end

  test 'validate password confirmation' do
    user = User.new name: 'Test user', email: 'test@akshi.com', password: 'test',
                    password_confirmation: 'invalid', mobile: '9500774799'
    assert !user.save
    assert user.errors[:password_confirmation].include?("doesn't match Password")
    user.password_confirmation = 'test'
    assert user.save
  end

  test 'authentication' do
    user = users(:gg)
    assert user.authenticate('password')
    assert !user.authenticate('invalid')
  end

  test 'relationships' do
    users(:gg).packages.include?(:neet)
    users(:gg).packages.include?(:iit)
    users(:akshi).packages.include?(:jipmer)
    users(:akshi).packages.include?(:iit)
  end

  test 'enrolled?' do
    assert users(:gg).enrolled?(packages(:neet))
    assert !users(:gg).enrolled?(packages(:iit))
  end

end
