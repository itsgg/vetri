# == Schema Information
#
# Table name: assigments
#
#  id          :integer          not null, primary key
#  test_id     :integer          not null
#  question_id :integer          not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'test_helper'

class AssigmentTest < ActiveSupport::TestCase

  test 'validations' do
    assignment = Assigment.new
    assert !assignment.save
    assert assignment.errors[:test].include?("can't be blank")
    assert assignment.errors[:question].include?("can't be blank")
  end

end
