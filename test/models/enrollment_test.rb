# == Schema Information
#
# Table name: enrollments
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  package_id :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'test_helper'

class EnrollmentTest < ActiveSupport::TestCase

  test 'validate presence' do
    enrollment = Enrollment.new
    assert !enrollment.save
    assert enrollment.errors[:user].include?("can't be blank")
    assert enrollment.errors[:package].include?("can't be blank")
  end

end
