# == Schema Information
#
# Table name: images
#
#  id         :integer          not null, primary key
#  file       :string(255)      not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'test_helper'

class ImageTest < ActiveSupport::TestCase

  test 'validations' do
    image = Image.new
    assert !image.save
    assert image.errors[:file].include?("can't be blank")
  end

end
