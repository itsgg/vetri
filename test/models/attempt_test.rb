# == Schema Information
#
# Table name: attempts
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  test_id    :integer          not null
#  score      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'test_helper'

class AttemptTest < ActiveSupport::TestCase
  setup do
    @attempt = attempts(:one)
  end

  test 'validations' do
    attempt = Attempt.new
    assert !attempt.save
    assert attempt.errors[:user].include?("can't be blank")
    assert attempt.errors[:test].include?("can't be blank")
  end

  test 'calculate_score!' do
    @attempt.calculate_score!
    assert_equal 1, @attempt.score
  end

  test 'total' do
    assert_equal 4, @attempt.total
  end

  test 'correct' do
    assert_equal 1, @attempt.correct
  end

  test 'incorrect' do
    assert_equal 3, @attempt.incorrect
  end

  test 'correct_percent' do
    assert_equal 25, @attempt.correct_percent
  end

  test 'incorrect_percent' do
    assert_equal 75, @attempt.incorrect_percent
  end

end
