module ApplicationHelper

  def bootstrap_class_for(flash_type)
    flash_class_hash = HashWithIndifferentAccess.new success: 'alert-success', error: 'alert-danger',
                                                     alert: 'alert-warning', notice: 'alert-info'
    flash_class_hash[flash_type] || flash_type.to_s
  end

  def flash_messages(opts = {})
    output_message = String.new
    flash.each do |message_type, message|
      html = <<-HTML
        <div class="alert #{bootstrap_class_for(message_type)} alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          #{message}
        </div>
      HTML
      output_message += html
    end
    output_message.html_safe
  end

  # XXX: This is really a hack
  def menu_active?(item)
    case item
    when 'home'
      return current_page?(root_path) || request.path.include?('/packages/')
    when 'login'
      return current_page?(login_path)
    when 'register'
      return current_page?(register_path)
    when 'account'
      return current_page?(edit_user_path(current_user))
    when 'my-packages'
      return current_page?(packages_path) && params[:my].present?
    when 'questions'
      return controller_name == 'questions'
    when 'users'
      return controller_name == 'users'
    end
  end

  def package_menu_active?(item)
    case item
    when 'overview'
      return !request.path.include?('/tests') &&
             !request.path.include?('/announcements') &&
             !request.path.include?('/topics') &&
             !request.path.include?('/lessons') &&
             !request.path.include?('/classrooms') &&
             !request.path.include?('/users')
    when 'discussion'
      return request.path.include?('/topics')
    when 'lessons'
      return request.path.include?('/lessons')
    when 'live_classes'
      return request.path.include?('/classrooms')
    when 'tests'
      return request.path.include?('/tests')
    when 'announcements'
      return request.path.include?('/announcements')
    when 'users'
      return request.path.include?('/users')
    end
  end

  def star_rating(package)
    rating = package.rating
    normalized_rating = normalize_to_point_five(rating)
    full_stars = 0
    half_stars = 0
    no_stars = 0
    star_index = 0
    content_tag 'span' do
      full_stars = normalized_rating.floor
      if BigDecimal.new(normalized_rating, 2).frac == BigDecimal.new('0.5', 2)
        half_stars = 1
      end
      no_stars = 5 - full_stars - half_stars
      full_stars.times do
        star_index += 1
        concat link_to(fa_icon('star 2x'), rate_package_path(package, value: star_index), class: 'text-warning rating-star', title: t('actions.click_to_rate'), method: :put)
      end
      half_stars.times do
        star_index += 1
        concat link_to(fa_icon('star-half-full 2x'), rate_package_path(package, value: star_index), class: 'text-warning rating-star', title: t('actions.click_to_rate'), method: :put)
      end
      no_stars.times do
        star_index += 1
        concat link_to(fa_icon('star-o 2x'), rate_package_path(package, value: star_index), class: 'text-warning rating-star', title: t('actions.click_to_rate'), method: :put)
      end
    end
  end

  def item_index(index, page, per_page)
    page ||= 1
    per_page * (page.to_i - 1) + index + 1
  end

  def normalize_to_point_five(rating)
    (rating * 2).round / 2.0
  end
end
