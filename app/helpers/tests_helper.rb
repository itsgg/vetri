module TestsHelper
  def question_badge_color(index, current_index, attempted)
    if index == current_index || current_index.blank? && index == 0
      return 'badge-primary'
    end
    if attempted.present?
      return 'badge-success'
    else
      return 'badge-warning'
    end
    'badge-light'
  end
end
