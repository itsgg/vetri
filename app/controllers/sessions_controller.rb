class SessionsController < ApplicationController

  def new
    @meta_title = meta_title t('menus.login')
  end

  def create
    user = User.find_by_email(params[:email])
    if user.present? && user.authenticate(params[:password])
      session[:current_user_id] = user.id
      flash[:notice] = t('messages.loggedin')
      redirect_to root_path
    else
      flash.now[:error] = t('messages.invalid_credentials')
      render :new
    end
  end

  def destroy
    session[:current_user_id] = nil
    flash[:notice] = t('messages.loggedout')
    redirect_to root_path
  end

end
