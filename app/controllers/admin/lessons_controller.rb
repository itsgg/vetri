class Admin::LessonsController < AdminController
  before_action :load_package
  before_action :load_lesson, only: [:edit, :update, :destroy, :update_video_uuid, :delete_video]

  def new
    @lesson = @package.lessons.new
  end

  def edit
  end

  def create
    @lesson = @package.lessons.new(lesson_params)
    if @lesson.save
      flash[:notice] = t('messages.created')
      redirect_to package_lessons_path(@package)
    else
      flash.now[:error] = @lesson.errors.full_messages.first
      render :new
    end
  end

  def update
    if @lesson.update_attributes(lesson_params.merge(package: @package))
      flash[:notice] = t('messages.updated')
      redirect_to package_lessons_path(@package)
    else
      flash.now[:error] = @lesson.errors.full_messages.first
      render :new
    end
  end

  def destroy
    @lesson.destroy
    flash[:notice] = t('messages.deleted')
    redirect_to package_lessons_path(@package)
  end

  private
  def load_package
    @package = Package.unscoped.find(params[:package_id])
  end

  def load_lesson
    @lesson = @package.lessons.find(params[:id])
  end

  def lesson_params
    params.require(:lesson).permit(:title, :description, :video_uuid)
  end
end
