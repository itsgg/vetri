class Admin::QuestionsController < AdminController

  def index
    if params[:tags].present?
      @questions = Question.tagged_with(params[:tags])
    else
      @questions = Question
    end
    if params[:content].present?
      @questions = @questions.where(['content like ?', "%#{params[:content]}%"])
    end
    @questions = @questions.paginate(page: params[:page])
  end

  def show
    @question = Question.find(params[:id])
  end

  def new
    @question = Question.new
  end

  def create
    @question = Question.new(question_params)
    if @question.save
      correct_answer = @question.answers[params[:question][:answer_index].to_i]
      @question.update_column 'answer_id', correct_answer.id
      flash[:notice] = t('messages.created')
      redirect_to admin_question_path(@question)
    else
      flash.now[:error] = @question.errors.full_messages.first
      render :new
    end
  end

  def update_explanation_uuid
    @question = Question.find(params[:id])
    @question.update_attribute :explanation_uuid, params[:video_uuid]
    head :ok
  end

  def destroy_explanation_uuid
    @question = Question.find(params[:id])
    @question.update_attribute :explanation_uuid, nil
    redirect_to edit_admin_question_path(@question)
  end

  def edit
    @question = Question.find(params[:id])
  end

  def update
    @question = Question.find(params[:id])
    if @question.update_attributes(question_params)
      flash[:notice] = t('messages.updated')
      correct_answer = @question.answers[params[:question][:answer_index].to_i]
      @question.update_column 'answer_id', correct_answer.id
      redirect_to admin_question_path(@question)
    else
      flash.now[:error] = @question.errors.full_messages.first
      render :edit
    end
  end

  def destroy
    Question.find(params[:id]).destroy
    flash[:notice] = t('messages.deleted')
    redirect_to admin_questions_path
  end

  private
  def question_params
    params.require(:question).permit(:content, :explanation_uuid, tag_list: [], answers_attributes: [:content, :_destroy, :id])
  end

end
