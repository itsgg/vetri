class Admin::TopicsController < AdminController
  before_action :load_topic, only: [:edit, :update, :destroy]

  def new
    @topic = Topic.new
  end

  def edit
  end

  def create
    @topic = Topic.new(post_params)
    if @topic.save
      flash[:notice] = t('messages.created')
      redirect_to topics_path
    else
      flash.now[:error] = @topic.errors.full_messages.first
      render :new
    end
  end

  def update
    if @topic.update_attributes(post_params)
      flash[:notice] = t('messages.updated')
      redirect_to posts_path
    else
      flash.now[:error] = @topic.errors.full_messages.first
      render :edit
    end
  end

  def destroy
    @topic.destroy
    flash[:notice] = t('messages.deleted')
    redirect_to posts_path
  end

  private
  def load_topic
    @topic = Topic.find(params[:id])
  end

  def post_params
    params.require(:topic).permit(:title, :content)
  end
end
