class Admin::PackagesController < AdminController
  before_action :load_package, only: [:edit, :update, :destroy, :enroll, :unenroll]

  def new
    @package = Package.new
  end

  def edit
  end

  def enroll
    user = User.find_by_email(params[:email])
    if user.present?
      unless @package.users.include?(user)
        flash[:notice] = 'User successfully added to package'
        @package.users << user
      else
        flash[:error] = 'User already subscribed to package'
      end
    else
      flash[:error] = 'User not found'
    end
    redirect_to users_package_path(@package)
  end

  def unenroll
    user = User.find(params[:user_id])
    if user.present?
      @package.users.delete user
      flash[:notice] = 'User unenrolled successfully'
    end
    redirect_to users_package_path(@package)
  end

  def create
    @package = Package.new(package_params)
    if @package.save
      flash[:notice] = t('messages.created')
      redirect_to packages_path
    else
      flash.now[:error] = @package.errors.full_messages.first
      render :new
    end
  end

  def update
    if @package.update_attributes(package_params)
      flash[:notice] = t('messages.updated')
      redirect_to package_path(@package)
    else
      flash.now[:error] = @package.errors.full_messages.first
      render :edit
    end
  end

  def destroy
    @package.destroy
    flash[:notice] = t('messages.deleted')
    redirect_to packages_path
  end

  private
  def package_params
    params.require(:package).permit(:name, :subtitle, :description, :published, :has_test,
                                    :cost, :has_announcement, :has_discussion,
                                    :has_lesson, :has_live_class, :profile)
  end

  def load_package
    @package = Package.unscoped.find(params[:id])
  end

end
