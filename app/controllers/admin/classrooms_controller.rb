class Admin::ClassroomsController < AdminController
  before_action :load_package
  before_action :load_classroom, only: [:edit, :update, :destroy]

  def new
    @classroom = @package.classrooms.new
  end

  def edit
  end

  def create
    @classroom = @package.classrooms.new(classroom_params)
    if @classroom.save
      flash[:notice] = t('messages.created')
      redirect_to package_classrooms_path(@package)
    else
      flash.now[:error] = @classroom.errors.full_messages.first
      render :new
    end
  end

  def update
    if @classroom.update_attributes(classroom_params.merge(package: @package))
      flash[:notice] = t('messages.updated')
      redirect_to package_classrooms_path(@package)
    else
      flash.now[:error] = @classroom.errors.full_messages.first
      render :edit
    end
  end

  def destroy
    @classroom.destroy
    flash[:notice] = t('messages.deleted')
    redirect_to package_classrooms_path(@package)
  end

  private
  def load_package
    @package = Package.unscoped.find(params[:package_id])
  end

  def load_classroom
    @classroom = @package.classrooms.find(params[:id])
  end

  def classroom_params
    params.require(:classroom).permit(:name, :description, :chronic_start_time, :presentation)
  end

end
