class Admin::UsersController < AdminController

  def index
    @users = User
    if params[:users_q].present?
      @users = User.where('name like ? or email like ?', "%#{params[:users_q]}%", "%#{params[:users_q]}%")
    end
    @users = @users.paginate(page: params[:page])
  end
end
