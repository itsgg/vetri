class Admin::AnnouncementsController < AdminController
  before_action :load_package
  before_action :load_announcement, only: [:edit, :update, :destroy]

  def new
    @announcement = @package.announcements.new
  end

  def edit
  end

  def create
    @announcement = @package.announcements.new(announcement_params)
    if @announcement.save
      flash[:notice] = t('messages.created')
      redirect_to package_announcements_path(@package)
    else
      flash.now[:error] = @announcement.errors.full_messages.first
      render :new
    end
  end

  def update
    if @announcement.update_attributes(announcement_params.merge(package: @package))
      flash[:notice] = t('messages.updated')
      redirect_to package_announcements_path(@package)
    else
      flash.now[:error] = @announcement.errors.full_messages.first
      render :edit
    end
  end

  def destroy
    @announcement.destroy
    flash[:notice] = t('messages.deleted')
    redirect_to package_announcements_path(@package)
  end

  private
  def load_package
    @package = Package.unscoped.find(params[:package_id])
  end

  def load_announcement
    @announcement = @package.announcements.find(params[:id])
  end

  def announcement_params
    params.require(:announcement).permit(:title, :content, tag_list: [])
  end
end
