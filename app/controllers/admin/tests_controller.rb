class Admin::TestsController < AdminController
  before_action :load_package
  before_action :load_test, only: [:edit, :update, :destroy, :add_question, :select_questions, :remove_question, :preview, :questions]

  def new
    @test = @package.tests.new
  end

  def edit
  end

  def create
    @test = @package.tests.new(test_params)
    if @test.save
      flash[:notice] = t('messages.created')
      redirect_to package_tests_path(@package)
    else
      flash.now[:error] = @test.errors.full_messages.first
      render :new
    end
  end

  def update
    if @test.update_attributes(test_params.merge(package: @package))
      flash[:notice] = t('messages.updated')
      redirect_to package_tests_path(@package)
    else
      flash.now[:error] = @test.errors.full_messages.first
      render :edit
    end
  end

  def destroy
    @test.destroy
    flash[:notice] = t('messages.deleted')
    redirect_to package_tests_path(@package)
  end

  def questions
    @questions = @test.questions.paginate(page: params[:page])
  end

  def preview
    @questions = @test.questions
    respond_to do |format|
      format.pdf do
        render pdf: "preview",
               javascript_delay: 10000,
               header: { right: '[page] of [topage]' },
               template: 'admin/tests/preview.html.erb',
               layout: 'pdf.html.erb'
      end
    end
  end

  def add_question
    @question = Question.find(params[:question_id])
    @test.questions << @question unless @test.questions.include?(@question)
    flash[:notice] = t('messages.added')
    redirect_to questions_admin_package_test_path(@package, @test)
  end

  def remove_question
    @question = Question.find(params[:question_id])
    @test.questions.delete(@question)
    flash[:notice] = t('messages.removed')
    redirect_to questions_admin_package_test_path(@package, @test)
  end

  def select_questions
    if params[:tags].present?
      @questions = Question.tagged_with(params[:tags])
    else
      @questions = Question
    end
    if params[:content].present?
      @questions = @questions.where(['content like ?', "%#{params[:content]}%"])
    end
    @questions = @questions.paginate(page: params[:page])
  end

  private
  def load_package
    @package = Package.unscoped.find(params[:package_id])
  end

  def load_test
    @test = @package.tests.unscoped.find(params[:id])
  end

  def test_params
    params.require(:test).permit(:title, :description, :positive_marks, :negative_marks,
                                 :time_limit_in_mins, :dynamic, :questions_count, :published, :reattempt, tag_list: [])
  end
end
