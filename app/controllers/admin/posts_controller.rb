class Admin::PostsController < AdminController
  before_action :load_post, only: [:edit, :update, :destroy]

  def new
    @post = Post.new
  end

  def edit
  end

  def create
    @post = Post.new(post_params)
    if @post.save
      flash[:notice] = t('messages.created')
      redirect_to posts_path
    else
      flash.now[:error] = @post.errors.full_messages.first
      render :new
    end
  end

  def update
    if @post.update_attributes(post_params)
      flash[:notice] = t('messages.updated')
      redirect_to posts_path
    else
      flash.now[:error] = @post.errors.full_messages.first
      render :edit
    end
  end

  def destroy
    @post.destroy
    flash[:notice] = t('messages.deleted')
    redirect_to posts_path
  end

  private
  def load_post
    @post = Post.find(params[:id])
  end

  def post_params
    params.require(:post).permit(:title, :content, tag_list: [])
  end
end
