class ChatsController < ApplicationController
  before_action :require_user
  before_action :require_enrolled_user
  before_action :load_package
  before_action :load_classroom

  def create
    @chat = @classroom.chats.new(chat_params.merge(user_id: current_user.id))
    @chat.save
  end

  private
  def load_package
    @package = Package.unscoped.find(params[:package_id])
    @meta_title = @package.name
  end

  def load_classroom
    @classroom = @package.classrooms.find(params[:classroom_id])
  end

  def chat_params
    params.require(:chat).permit(:message)
  end
end
