class RepliesController < ApplicationController
  before_action :require_user
  before_action :require_enrolled_user
  before_action :load_package
  before_action :load_topic

  def new
    @reply = @topic.replies.new
  end

  def create
    @reply = @topic.replies.new(replies_params.merge(user_id: current_user.id))
    if @reply.save
      flash[:notice] = t('messages.created')
      redirect_to package_topic_path(@package, @topic)
    else
      flash.now[:error] = @reply.errors.full_messages.first
      render :new
    end
  end

  def destroy
    @reply = @topic.replies.find(params[:id])
    if current_user.admin?
      @reply.destroy
      flash[:notice] = t('messages.deleted')
    else
      flash[:error] = t('messages.unauthorized')
    end
    redirect_to package_topic_path(@package, @topic)
  end

  private
  def load_package
    @package = Package.unscoped.find(params[:package_id])
    @meta_title = @package.name
  end

  def load_topic
    @topic = @package.topics.find(params[:topic_id])
  end

  def replies_params
    params.require(:reply).permit(:content)
  end

end
