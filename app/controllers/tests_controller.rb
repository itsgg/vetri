class TestsController < ApplicationController
  before_action :require_user
  before_action :require_enrolled_user
  before_action :load_package
  before_action :load_test, only: [:show, :start, :attend, :review, :select, :complete, :result, :reattempt]

  def index
    if admin?
      @tests = Test.unscoped.where(package: @package).paginate(page: params[:page])
    else
      @tests = @package.tests.paginate(page: params[:page])
    end
  end

  def show
    @meta_title = meta_title @test.title
  end

  def start
    @test.generate_questions!
    redirect_to attend_package_test_path(@package, @test)
  end

  def attend
    @meta_title = meta_title @test.title
    @questions = @test.questions
    @attempt = @test.attempts.find_or_create_by(user: current_user)
    if params[:index].blank?
      @question = @questions[0]
    else
      @question = @questions[params[:index].to_i]
    end
  end

  def review
    @meta_title = meta_title @test.title
    @questions = @test.questions
    @attempt = @test.attempts.find_or_create_by(user: current_user)
    if params[:index].blank?
      @question = @questions.first
    else
      @question = @questions[params[:index].to_i]
    end
  end

  def select
    respond_to do |format|
      format.json do
        question = @test.questions.find(params[:question_id])
        answer = question.answers.find(params[:answer_id])
        attempt = @test.attempts.find_or_create_by(user: current_user)
        attempt.responses.where(question: question).delete_all
        attempt.responses.find_or_create_by!(question: question, answer: answer)
        head :ok
      end
    end
  rescue
  end

  def complete
    attempt = @test.attempts.find_or_create_by(user: current_user)
    attempt.calculate_score!
    flash[:notice] = 'Test completed successfully'
    redirect_to result_package_test_path(@package, @test)
  end

  def result
    @meta_title = meta_title @test.title
    @attempt = @test.attempt(current_user.id)
    @attempts = @test.attempts.sort_by(&:score).reverse
    @rank = @test.rank(current_user.id)
  end

  def reattempt
    if @test.reattempt?
      @test.attempt(current_user.id).destroy
      redirect_to package_test_path(@package, @test)
    else
      flash[:error] = 'Maximum attempt reached for this test.'
      redirect_to result_package_test_path(@package, @test)
    end
  end

  private
  def load_package
    @package = Package.unscoped.find(params[:package_id])
    @meta_title = @package.name
  end

  def load_test
    @test = @package.tests.find(params[:id])
  end
end
