class PackagesController < ApplicationController
  before_action :require_user, only: [:enroll, :show, :users]
  before_action :load_package, only: [:show, :enroll, :rate, :users, :payment_callback]
  skip_before_action :verify_authenticity_token, only: [:payment_callback]

  def index
    @meta_title = meta_title t('meta_title')
    if params['my'].present?
      @packages = current_user.packages
    else
      if admin?
        @packages = Package.unscoped
      else
        @packages = Package
      end
    end
    if params[:q].present?
      @packages = @packages.where(['name like ?', "%#{params[:q]}%"])
    end
    @packages = @packages.paginate(page: params[:page])
    @posts = Post.order(created_at: :desc).last(5)
    @classrooms = Classroom.limit(5)
  end

  def show
  end

  def users
    @users = @package.users.paginate(page: params[:page])
  end

  def rate
    @package.rate!(params['value'].to_i)
    redirect_back(fallback_location: package_path(@package))
  end

  def payment_callback
    notification = PayuIndia::Notification.new(request.query_string, options = {key: ENV['PAYU_KEY'], salt: ENV['PAYU_SALT'], params: params})
    if notification.complete?
      current_user.packages << @package
      flash[:notice] = t('packages.payment_successful')
    else
      flash[:error] = "#{notification.message}"
    end
    redirect_to package_path(@package)
  end

  def enroll
    current_user.packages << @package
    flash[:notice] = t('packages.enrolled')
    redirect_to package_path(@package)
  end

  private
  def load_package
    @package = Package.unscoped.find(params[:id])
    @meta_title = @package.name
  end
end
