class ClassroomsController < ApplicationController
  before_action :require_user
  before_action :require_enrolled_user
  before_action :load_package
  before_action :load_classroom, only: [:show, :prepare_presentation, :initialize_live]

  def index
    @classrooms = @package.classrooms.paginate(page: params[:page])
  end

  def show
    @chats = @classroom.chats.all
    @chat = @classroom.chats.new
    render layout: 'layouts/presentation'
  end

  def prepare_presentation
    @classroom.process_presentation!
    flash[:notice] = 'Preparing presentation'
    redirect_to package_classrooms_path(@package)
  end

  def initialize_live
    @classroom.update_opentok_credentials!
    flash[:notice] = 'Preparing live session'
    redirect_to package_classrooms_path(@package)
  end

  private
  def load_package
    @package = Package.unscoped.find(params[:package_id])
    @meta_title = @package.name
  end

  def load_classroom
    @classroom = @package.classrooms.find(params[:id])
    @meta_title = @classroom.name
  end
end
