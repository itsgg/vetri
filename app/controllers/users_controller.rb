class UsersController < ApplicationController

  def new
    @meta_title = meta_title t('menus.register')
    @user = User.new
  end

  def edit
    @user = User.find(params[:id])
  end

  def shadow
    user = User.find(params[:id])
    session[:current_user_id] = user.id
    flash[:notice] = t('messages.loggedin')
    redirect_to root_path
  end

  def update
    @user = current_user
    if @user.update_attributes(user_params)
      flash[:notice] = t('messages.updated')
      redirect_to edit_user_path(@user)
    else
      flash.now[:error] = @user.errors.full_messages.first
      render :new
    end
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash[:notice] = t('messages.registered')
      session[:current_user_id] = @user.id
      redirect_to root_path
    else
      flash.now[:error] = @user.errors.full_messages.first
      render :new
    end
  end

  private
  def user_params
    params.require(:user).permit(:email, :name, :password, :mobile, :password_confirmation, :avatar)
  end

end
