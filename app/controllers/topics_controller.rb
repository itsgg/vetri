class TopicsController < ApplicationController
  before_action :require_user
  before_action :require_enrolled_user
  before_action :load_package

  def index
    @topics = @package.topics.paginate(page: params[:page])
  end

  def new
    @topic = @package.topics.new
  end

  def show
    @topic = @package.topics.find(params[:id])
    @replies = @topic.replies.paginate(page: params[:page])
  end

  def create
    @topic = @package.topics.new(topics_params.merge(user_id: current_user.id))
    if @topic.save
      flash[:notice] = t('messages.created')
      redirect_to package_topic_path(@package, @topic)
    else
      flash.now[:error] = @topic.errors.full_messages.first
      render :new
    end
  end

  def destroy
    @topic = @package.topics.find(params[:id])
    if current_user.admin?
      @topic.destroy
      flash[:notice] = t('messages.deleted')
    else
      flash[:error] = t('messages.unauthorized')
    end
    redirect_to package_topics_path(@package)
  end

  private
  def load_package
    @package = Package.unscoped.find(params[:package_id])
    @meta_title = @package.name
  end

  def topics_params
    params.require(:topic).permit(:title, :content)
  end
end
