class AnnouncementsController < ApplicationController
  before_action :require_user
  before_action :require_enrolled_user
  before_action :load_package

  def index
    @announcements = @package.announcements
    if params[:tag].present?
      @announcements = @announcements.tagged_with([params[:tag]], any: true)
    end
    @announcements = @announcements.paginate(page: params[:page])
    @tags = @package.announcements.tag_counts.sort_by(&:count).reverse
  end

  private
  def load_package
    @package = Package.unscoped.find(params[:package_id])
    @meta_title = @package.name
  end
end
