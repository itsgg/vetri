class ApplicationController < ActionController::Base
  # before_action :beta_authenticate
  protect_from_forgery with: :exception

  helper_method :current_user, :logged_in?, :admin?

  BRAND_NAME = 'Vetri'.freeze

  protected
  def meta_title(title)
    [title, BRAND_NAME].reject(&:empty?).join(' | ')
  end

  def current_user
    User.find_by_id(session[:current_user_id])
  end

  def logged_in?
    current_user.present?
  end

  def admin?
    logged_in? && current_user.admin?
  end

  def require_user
    unless logged_in?
      flash[:error] = t('messages.login_required')
      redirect_to login_path
    end
  end

  def require_admin
    unless admin?
      flash[:error] = t('messages.unauthorized')
      redirect_to root_path
    end
  end

  def beta_authenticate
    if Rails.env.production?
      authenticate_or_request_with_http_basic do |username, password|
        username == 'admin' && password == 'letmein'
      end
    end
  end

  def require_enrolled_user
    package = Package.unscoped.find(params[:package_id])
    unless current_user.enrolled?(package)
      flash[:error] = t('packages.enroll_to_access')
      redirect_to package_path(package)
    end
  end
end
