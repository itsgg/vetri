class LessonsController < ApplicationController
  before_action :require_user
  before_action :require_enrolled_user
  before_action :load_package

  def index
    @lessons = @package.lessons.paginate(page: params[:page])
  end

  private
  def load_package
    @package = Package.unscoped.find(params[:package_id])
    @meta_title = @package.name
  end
end
