class ImagesController < ApplicationController
  def upload
    image = Image.new
    image.file = params[:file]
    if image.save
      render json: { status: 'OK', link: image.file.url }
    else
      render json: { status: 'Error'}
    end
  end
end
