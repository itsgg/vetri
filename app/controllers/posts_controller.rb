class PostsController < ApplicationController

  def index
    @meta_title = meta_title t('meta_title')
    @posts = Post
    if params[:tag].present?
      @posts = @posts.tagged_with([params[:tag]], any: true)
    end
    @posts = @posts.paginate(page: params[:page])
    @tags = Post.tag_counts.sort_by(&:count).reverse
  end

  def show
    @post = Post.find(params[:id])
  end

end
