class Api::PackagesController < Api::ApplicationController

  def index
    paginate json: Package.all
  end

end
