# == Schema Information
#
# Table name: packages
#
#  id               :integer          not null, primary key
#  name             :string(255)      not null
#  description      :text(65535)
#  rating           :float(24)        default(0.0)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  profile          :string(255)
#  published        :boolean          default(FALSE)
#  rating_count     :integer          default(0)
#  rating_value     :integer          default(0)
#  has_test         :boolean          default(TRUE), not null
#  has_announcement :boolean          default(TRUE), not null
#  has_discussion   :boolean          default(TRUE), not null
#  has_lesson       :boolean          default(TRUE), not null
#  has_live_class   :boolean          default(TRUE), not null
#  cost             :decimal(10, )    default(0), not null
#  subtitle         :string(255)
#

class Package < ApplicationRecord
  acts_as_taggable

  has_many :enrollments
  has_many :users, through: :enrollments
  has_many :tests
  has_many :announcements
  has_many :topics
  has_many :lessons
  has_many :classrooms, dependent: :destroy

  validates :name, presence: true

  default_scope { where(published: true).order(created_at: :desc) }

  self.per_page = 12

  mount_uploader :profile, ProfileUploader

  def rate!(value)
    self.rating_count += 1
    self.rating_value = self.rating_value + value
    self.rating = self.rating_value/self.rating_count.to_f
    self.save!
  end

  def free?
    self.cost == 0
  end
end
