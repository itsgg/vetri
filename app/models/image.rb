# == Schema Information
#
# Table name: images
#
#  id         :integer          not null, primary key
#  file       :string(255)      not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Image < ApplicationRecord

  mount_uploader :file, ImageUploader

  validates :file, presence: true
  
end
