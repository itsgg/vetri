# == Schema Information
#
# Table name: tests
#
#  id                 :integer          not null, primary key
#  title              :string(255)      not null
#  description        :text(65535)      not null
#  package_id         :integer          not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  time_limit_in_mins :integer
#  negative_marks     :integer          default(0)
#  positive_marks     :integer          default(1)
#  dynamic            :boolean          default(FALSE)
#  questions_count    :integer          default(10)
#  published          :boolean          default(FALSE)
#

class Test < ApplicationRecord

  acts_as_taggable

  before_save :check_question_size
  before_save :check_tags

  belongs_to :package
  has_many :assigments
  has_many :questions, through: :assigments

  validates :title, presence: true
  validates :package, presence: true

  has_many :attempts, dependent: :destroy

  default_scope { where(published: true) }

  self.per_page = 10

  def generate_questions!
    if self.dynamic?
      questions = Question.tagged_with(self.tag_list)
      self.questions = questions.sample(self.questions_count).sort
      self.save!
    end
    self.questions
  end

  def attempt(user_id)
    attempts.where(user_id: user_id).last
  end

  def rank(user_id)
    attempts.sort_by(&:score).reverse.index(attempt(user_id)) + 1
  end

  def complete?(user_id)
    attempt(user_id).try(:score).present?
  end

  def started_at(user_id)
    attempt(user_id).created_at
  end

  private
  def check_tags
    if self.dynamic? && self.tag_list.blank?
      self.errors.add(:base, I18n.t('test.tags_required'))
      throw :abort
    end
  end

  def check_question_size
    if self.dynamic? && self.questions_count.blank?
      self.errors.add(:questions_count, I18n.t('test.is_required'))
      throw :abort
    end
  end
end
