# == Schema Information
#
# Table name: attempts
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  test_id    :integer          not null
#  score      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Attempt < ApplicationRecord
  belongs_to :user
  belongs_to :test

  validates :user, presence: true
  validates :test, presence: true

  has_many :responses, dependent: :destroy

  def calculate_score!
    self.score = 0
    self.responses.each do |response|
      if response.answer == response.question.answer
        self.score += self.test.positive_marks
      else
        self.score -= self.test.negative_marks
      end
    end
    self.save!
  end

  def total
    self.test.questions.count * self.test.positive_marks
  end

  def incorrect
    total - self.score
  end

  def correct
    total - incorrect
  end

  def correct_percent
    correct/total.to_f * 100
  end

  def incorrect_percent
    incorrect/total.to_f * 100
  end
end
