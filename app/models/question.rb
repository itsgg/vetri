# == Schema Information
#
# Table name: questions
#
#  id               :integer          not null, primary key
#  content          :text(65535)      not null
#  answer_id        :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  explanation_uuid :string(255)
#

class Question < ApplicationRecord
  acts_as_taggable
  validates :content, presence: true

  has_many :answers, dependent: :destroy
  has_many :assigments
  has_many :tests, through: :assigments

  # TODO: Support multiple answers
  # Correct answer
  belongs_to :answer, optional: true

  attr_accessor :answer_index

  accepts_nested_attributes_for :answers, allow_destroy: true

  self.per_page = 12

  def attempted?(test, user)
    attempt = test.attempts.find_by(user: user)
    attempt.responses.where(question: self).present?
  end
end
