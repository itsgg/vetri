# == Schema Information
#
# Table name: assigments
#
#  id          :integer          not null, primary key
#  test_id     :integer          not null
#  question_id :integer          not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Assigment < ApplicationRecord
  belongs_to :test
  belongs_to :question

  validates :test, presence: true
  validates :question, presence: true
end
