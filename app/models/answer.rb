# == Schema Information
#
# Table name: answers
#
#  id          :integer          not null, primary key
#  content     :text(65535)      not null
#  question_id :integer          not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Answer < ApplicationRecord

  belongs_to :question
  validates :content, presence: true

  def selected?(attempt, question)
    attempt.responses.where(question: question, answer: self).present?
  end

end
