# == Schema Information
#
# Table name: chats
#
#  id           :integer          not null, primary key
#  message      :text(65535)      not null
#  user_id      :integer          not null
#  classroom_id :integer          not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Chat < ApplicationRecord
  belongs_to :classroom
  belongs_to :user

  validates :message, presence: true
  validates :classroom, presence: true
  validates :user, presence: true

  default_scope { order(created_at: :desc) }
end
