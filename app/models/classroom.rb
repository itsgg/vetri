# == Schema Information
#
# Table name: classrooms
#
#  id                   :integer          not null, primary key
#  name                 :string(255)      not null
#  description          :text(65535)
#  start_time           :datetime
#  package_id           :integer          not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  opentok_session_id   :string(255)
#  opentok_token        :text(65535)
#  presentation         :string(255)
#  presentation_baseurl :string(255)
#  presentation_count   :integer          default(0), not null
#

class Classroom < ApplicationRecord
  belongs_to :package
  has_many :chats

  validates :name, presence: true
  validates :package, presence: true

  default_scope -> { where('classrooms.start_time > ?', Time.zone.now - 3.hours).order(start_time: :asc) }

  self.per_page = 12

  mount_uploader :presentation, PresentationUploader

  def chronic_start_time
    self.start_time
  end

  def presentation_basename
    File.basename(self.presentation.path, File.extname(self.presentation.path))
  end

  def chronic_start_time=(time)
    Chronic.time_class = Time.zone
    self.start_time = Chronic.parse(time) if time
  end

  def update_opentok_credentials!
    LiveSessionInitializerWorker.perform_async(self.id, ENV['OPEN_TOK_KEY'], ENV['OPEN_TOK_SECRET'])
  end

  def process_presentation!
    if self.presentation.present?
      DocumentConverterWorker.perform_async(self.id)
    end
  end
end
