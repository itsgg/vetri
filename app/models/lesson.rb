# == Schema Information
#
# Table name: lessons
#
#  id          :integer          not null, primary key
#  title       :string(255)      not null
#  description :text(65535)
#  video_uuid  :string(255)
#  package_id  :integer          not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Lesson < ApplicationRecord
  belongs_to :package

  validates :title, presence: true
  validates :package, presence: true

  self.per_page = 12
end
