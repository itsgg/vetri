# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  name            :string(255)      not null
#  email           :string(255)      not null
#  password_digest :string(255)      not null
#  admin           :boolean          default(FALSE)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  avatar          :string(255)
#  mobile          :string(255)
#

class User < ApplicationRecord
  attribute :avatar

  has_secure_password validations: false

  validates :name, presence: true
  validates :email, presence: true, uniqueness: true
  validates :password, confirmation: true, if: :validate_password?
  validates :mobile, presence: true

  has_many :enrollments
  has_many :packages, through: :enrollments

  mount_uploader :avatar, AvatarUploader

  self.per_page = 12

  def enrolled?(package)
    self.packages.include?(package) || self.admin?
  end

  protected
  def validate_password?
    password.present? || password_confirmation.present?
  end
end
