# == Schema Information
#
# Table name: topics
#
#  id         :integer          not null, primary key
#  title      :string(255)      not null
#  content    :text(65535)      not null
#  package_id :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#

class Topic < ApplicationRecord
  default_scope { order(created_at: :desc) }

  self.per_page = 10

  belongs_to :package

  validates :package, presence: true

  belongs_to :user

  has_many :replies
end
