# == Schema Information
#
# Table name: replies
#
#  id         :integer          not null, primary key
#  content    :text(65535)      not null
#  topic_id   :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#

class Reply < ApplicationRecord
  belongs_to :topic

  validates :content, presence: true
  validates :topic, presence: true

  belongs_to :user

  self.per_page = 10
end
