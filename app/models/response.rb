# == Schema Information
#
# Table name: responses
#
#  id          :integer          not null, primary key
#  attempt_id  :integer          not null
#  question_id :integer          not null
#  answer_id   :integer          not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Response < ApplicationRecord
  belongs_to :attempt
  belongs_to :question
  belongs_to :answer

  validates :attempt, presence: true
  validates :question, presence: true
  validates :answer, presence: true

end
