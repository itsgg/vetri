# == Schema Information
#
# Table name: announcements
#
#  id         :integer          not null, primary key
#  title      :string(255)      not null
#  content    :text(65535)      not null
#  package_id :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Announcement < ApplicationRecord
  acts_as_taggable

  default_scope { order(created_at: :desc) }

  self.per_page = 12

  belongs_to :package

  validates :package, presence: true
end
