class WhiteboardChannel < ApplicationCable::Channel
  def subscribed
    stream_from 'whiteboard_channel'
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def send_message(data)
    ActionCable.server.broadcast 'whiteboard_channel', data
  end
end
