class LiveSessionInitializerWorker
  include Sidekiq::Worker

  def perform(classroom_id, opentok_key, opentok_secret)
    classroom = Classroom.find(classroom_id)
    classroom.update_attribute :opentok_session_id, nil
    classroom.update_attribute :opentok_token, nil
    opentok = OpenTok::OpenTok.new opentok_key, opentok_secret
    session = opentok.create_session
    classroom.update_attribute :opentok_session_id, session.session_id
    classroom.update_attribute :opentok_token, session.generate_token
  end
end
