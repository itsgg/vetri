class DocumentConverterWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(classroom_id)
    classroom = Classroom.find(classroom_id)
    classroom.update_attribute :presentation_baseurl, nil
    file_path = classroom.presentation.path
    output_directory = File.dirname(file_path)
    basename = file_path.chomp(File.extname(file_path))
    Docsplit::extract_images file_path, output: output_directory
    url = classroom.presentation_url.split('.').first
    count = Dir.glob(basename + "*.png").count
    classroom.update_attribute :presentation_baseurl, url
    classroom.update_attribute :presentation_count, count
  end
end
