# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'turbolinks:load', ->
  $('.answer-block').click () ->
    data = $(this).data()
    $('.answer-card').removeClass('border-primary')
    $('.answer-card .answer-block').removeClass('selected')
    $("#answer-#{data.answerId}").addClass('border-primary')
    $("#answer-#{data.answerId} .answer-block").addClass('selected')
    $.ajax
      url: "/packages/#{data.packageId}/tests/#{data.testId}/select.json"
      method: 'PUT'
      data: { answer_id: data.answerId, question_id: data.questionId }

  unless $('#check-dynamic').is(':checked')
    $('.dynamic-input').hide()

  $('#check-dynamic').click () ->
    $('.dynamic-input').toggle $(this).is(':checked')
