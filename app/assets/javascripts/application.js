// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery3
//= require popper
//= require bootstrap
//= require jquery_ujs
//= require turbolinks
//= require nested_form_fields
//= require froala_editor.min
//= require plugins/image.min
//= require plugins/align.min
//= require plugins/colors.min
//= require plugins/table.min
//= require plugins/special_characters.min
//= require plugins/entities.min
//= require plugins/lists.min
//= require plugins/font_size.min
//= require plugins/url.min
//= require select2-full
//= require jquery.fullscreen
//= require_tree .
