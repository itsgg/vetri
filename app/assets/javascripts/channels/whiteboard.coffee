App.whiteboard = App.cable.subscriptions.create "WhiteboardChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    # Called when there's incoming data on the websocket for this channel
    if data.message.type == 'draw'
      addClick data.message.classroom, data.message.x, data.message.y, data.message.dragging
      redraw data.message.classroom

    if data.message.type == 'show'
      showPresentation(data.message.classroom, data.message.background_image)

    if data.message.type == 'clear'
      clearPresentation(data.message.classroom)

    if data.message.type == 'hide'
      hidePresentation(data.message.classroom)

    if data.message.type == 'chat'
      chat(data.message.classroom, data.message.message, data.message.user)

  send_message: (message) ->
    @perform 'send_message', { message: message }

clickX = []
clickY = []
clickDrag = []
drawing = false

chat = (classroom, message, user) ->
  chats = $('#chats').not(".chat_#{classroom}_#{user}")
  chats.prepend message

showPresentation = (classroom, backgroundImage) ->
  whiteboard = $(".whiteboard_#{classroom}.viewer")
  return unless whiteboard.is(':visible')
  whiteboard.css({
    'background': backgroundImage,
    'background-size': 'cover'
  });

hidePresentation = (classroom) ->
  whiteboard = $(".whiteboard_#{classroom}.viewer")
  return unless whiteboard.is(':visible')
  whiteboard.css({
    'background': '#fff'
  });

clearPresentation = (classroom) ->
  whiteboard = $(".whiteboard_#{classroom}.viewer")
  return unless whiteboard.is(':visible')
  canvas = whiteboard[0];
  context = canvas.getContext('2d');
  clickX.length = 0;
  clickY.length = 0;
  clickDrag.length = 0;
  context.clearRect(0, 0, canvas.width, canvas.height);

addClick = (classroom, x, y, dragging) ->
  return unless $(".whiteboard_#{classroom}.viewer").is(':visible')
  clickX.push x
  clickY.push y
  clickDrag.push dragging

redraw = (classroom) ->
  whiteboard = $(".whiteboard_#{classroom}.viewer")
  return unless whiteboard.is(':visible')
  context = whiteboard[0].getContext('2d')
  context.clearRect 0, 0, context.canvas.width, context.canvas.height
  context.strokeStyle = '#df4b26'
  context.lineJoin = 'round'
  context.lineWidth = 1
  for i in [0..clickX.length]
    context.beginPath()
    if clickDrag[i]
      context.moveTo(clickX[i - 1], clickY[i - 1])
    else
      context.moveTo(clickX[i] - 1, clickY[i])
    context.lineTo clickX[i], clickY[i]
    context.closePath()
    context.stroke()
