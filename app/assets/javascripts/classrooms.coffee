# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'turbolinks:load', (event) ->
  # Prevent scrolling when touching the canvas
  document.body.addEventListener 'touchstart', ((e) ->
    if e.target == canvas
      e.preventDefault()
    return
  ), false
  document.body.addEventListener 'touchend', ((e) ->
    if e.target == canvas
      e.preventDefault()
    return
  ), false
  document.body.addEventListener 'touchmove', ((e) ->
      if e.target == canvas
        e.preventDefault()
      return
    ), false
  document.body.addEventListener 'touchcancel', ((e) ->
    if e.target == canvas
      e.preventDefault()
    return
  ), false

  fullScreen = false
  $('#fullscreen').click (e) ->
    e.preventDefault()
    fullScreen = !fullScreen
    $(document).fullScreen(fullScreen)

  video = $('#video')
  sessionName = $('#video').data('sessionName')
  token = video.data('opentokToken')
  if token
    session = OT.initSession(video.data('opentokKey'), video.data('opentokSession'))
    session.on('streamCreated', (event) ->
      session.subscribe(event.stream, 'viewer', {
        insertMode: 'append',
        width: '320px',
        height: '250px'
      }, (error) ->
        if (error)
          console.log error
      )
    )
    session.connect(token, (error) ->
      if (error)
        console.log error
      else
        if $('#presenter').is(':visible')
          publisher = OT.initPublisher('presenter', {
            width: '320px',
            height: '250px',
            showControls: false,
            name: sessionName,
            insertMode: 'append'
          })
          session.publish(publisher, (error) ->
            if (error)
              console.log error
          )
    )

  whiteboard = $('#whiteboard.presenter')
  canvas = document.getElementById 'whiteboard'

  if whiteboard.is(':visible')
    context = whiteboard[0].getContext('2d')
    clickX = []
    clickY = []
    clickDrag = []
    presentationBaseurl = whiteboard.data('presentationBaseurl')
    presentationCount = whiteboard.data('presentationCount')
    classroomId = whiteboard.data('classroomId')
    showSlide = false
    drawing = false
    currentPresentation = 1

    loadSlide = ->
      if showSlide
        backgroundImage = "url('#{presentationBaseurl}_#{currentPresentation}.png')"
        App.whiteboard.send_message({type: 'show', classroom: classroomId, background_image: backgroundImage})
        $('#previousSlide').show()
        $('#nextSlide').show()
        whiteboard.css({
          'background': backgroundImage,
          'background-size': 'cover'
        })
      else
        App.whiteboard.send_message({type: 'hide', classroom: classroomId})
        $('#previousSlide').hide()
        $('#nextSlide').hide()
        whiteboard.css({
          'background': '#fff'
        })

    toggleNavs = ->
      if (currentPresentation == 1)
        $('#previousSlide').addClass('disabled')
      else
        $('#previousSlide').removeClass('disabled')
      if (currentPresentation == presentationCount)
        $('#nextSlide').addClass('disabled')
      else
        $('#nextSlide').removeClass('disabled')

    $('#togglePresentation').click (e) ->
      showSlide = !showSlide
      loadSlide()
      e.preventDefault()

    $('#clear').click (e) ->
      e.preventDefault()
      App.whiteboard.send_message({type: 'clear', classroom: classroomId})
      clickX.length = 0
      clickY.length = 0
      clickDrag.length = 0
      context.clearRect(0, 0, context.canvas.width, context.canvas.height)

    $('#nextSlide').click (e) ->
      currentPresentation++;
      loadSlide()
      toggleNavs()
      e.preventDefault()

    $('#previousSlide').click (e) ->
      currentPresentation--;
      loadSlide()
      toggleNavs()
      e.preventDefault()

    addClick = (x, y, dragging) ->
      clickX.push(x)
      clickY.push(y)
      clickDrag.push(dragging)

    redraw = ->
      context.clearRect(0, 0, context.canvas.width, context.canvas.height)
      context.strokeStyle = '#df4b26'
      context.lineJoin = 'round'
      context.lineWidth = 1
      for i in [0..clickX.length]
        context.beginPath()
        if clickDrag[i]
          context.moveTo(clickX[i - 1], clickY[i - 1])
        else
          context.moveTo(clickX[i] - 1, clickY[i])
        context.lineTo(clickX[i], clickY[i])
        context.closePath()
        context.stroke()

    whiteboard.mousedown (e) ->
      mouseX = e.pageX - @offsetLeft
      mouseY = e.pageY - @offsetTop
      drawing = true
      addClick(mouseX, mouseY, false)
      App.whiteboard.send_message({type: 'draw', classroom: classroomId, x: mouseX, y: mouseY, dragging: false})
      redraw()

    whiteboard.mousemove (e) ->
      if drawing
        mouseX = e.pageX - @offsetLeft
        mouseY = e.pageY - @offsetTop
        addClick(mouseX, mouseY, true)
        App.whiteboard.send_message({type: 'draw', classroom: classroomId, x: mouseX, y: mouseY, dragging: true})
        redraw()

    whiteboard.mouseup ->
      drawing = false

    whiteboard.mouseleave ->
      drawing = false

    getTouchPos = (canvasDom, touchEvent) ->
      rect = canvasDom.getBoundingClientRect()
      {
        x: touchEvent.touches[0].clientX - (rect.left)
        y: touchEvent.touches[0].clientY - (rect.top)
      }

    canvas.addEventListener 'touchstart', ((e) ->
      mousePos = getTouchPos(canvas, e)
      touch = e.touches[0]
      mouseEvent = new MouseEvent('mousedown',
        clientX: touch.clientX
        clientY: touch.clientY)
      canvas.dispatchEvent mouseEvent
      return
    ), false

    canvas.addEventListener 'touchend', ((e) ->
      mouseEvent = new MouseEvent('mouseup', {})
      canvas.dispatchEvent mouseEvent
      return
    ), false

    canvas.addEventListener 'touchmove', ((e) ->
      touch = e.touches[0]
      mouseEvent = new MouseEvent('mousemove',
        clientX: touch.clientX
        clientY: touch.clientY)
      canvas.dispatchEvent mouseEvent
      return
    ), false

    toggleNavs()
