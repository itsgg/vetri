class CreateEnrollments < ActiveRecord::Migration[5.1]
  def change
    create_table :enrollments do |t|
      t.references :user, null: false
      t.references :package, null: false
      t.timestamps
    end
  end
end
