class CreateTopics < ActiveRecord::Migration[5.1]
  def change
    create_table :topics do |t|
      t.string :title, null: false
      t.text :content, null: false
      t.references :package, null: false
      t.timestamps
    end
  end
end
