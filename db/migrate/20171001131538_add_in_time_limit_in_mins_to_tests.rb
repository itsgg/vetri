class AddInTimeLimitInMinsToTests < ActiveRecord::Migration[5.1]
  def change
    add_column :tests, :time_limit_in_mins, :integer, default: nil
  end
end
