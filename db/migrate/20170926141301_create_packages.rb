class CreatePackages < ActiveRecord::Migration[5.1]
  def change
    create_table :packages do |t|
      t.string :name, null: false
      t.text :description
      t.float :rating, default: 0
      t.timestamps
    end
  end
end
