class AddPresentationToClassrooms < ActiveRecord::Migration[5.1]
  def change
    add_column :classrooms, :presentation, :string
  end
end
