class AddReattemptOptionToTests < ActiveRecord::Migration[5.1]
  def change
    add_column :tests, :reattempt, :boolean, default: false
  end
end
