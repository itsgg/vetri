class AddHasAnnouncementToPackages < ActiveRecord::Migration[5.1]
  def change
    add_column :packages, :has_announcement, :boolean, null: false, default: true
  end
end
