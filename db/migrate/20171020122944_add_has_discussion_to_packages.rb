class AddHasDiscussionToPackages < ActiveRecord::Migration[5.1]
  def change
    add_column :packages, :has_discussion, :boolean, null: false, default: true
  end
end
