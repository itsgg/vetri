class AddOpentokSessionIdToClassrooms < ActiveRecord::Migration[5.1]
  def change
    add_column :classrooms, :opentok_session_id, :string
  end
end
