class AddDynamicToTests < ActiveRecord::Migration[5.1]
  def change
    add_column :tests, :dynamic, :boolean, default: false
  end
end
