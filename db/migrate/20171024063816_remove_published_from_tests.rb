class RemovePublishedFromTests < ActiveRecord::Migration[5.1]
  def change
    remove_column :tests, :published, :boolean
  end
end
