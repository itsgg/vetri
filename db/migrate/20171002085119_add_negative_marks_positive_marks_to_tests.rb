class AddNegativeMarksPositiveMarksToTests < ActiveRecord::Migration[5.1]
  def change
    add_column :tests, :negative_marks, :integer, default: 0
    add_column :tests, :positive_marks, :integer, default: 1
  end
end
