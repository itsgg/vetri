class AddHasLiveClassToPackage < ActiveRecord::Migration[5.1]
  def change
    add_column :packages, :has_live_class, :boolean, default: true, null: false
  end
end
