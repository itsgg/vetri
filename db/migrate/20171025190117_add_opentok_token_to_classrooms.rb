class AddOpentokTokenToClassrooms < ActiveRecord::Migration[5.1]
  def change
    add_column :classrooms, :opentok_token, :text
  end
end
