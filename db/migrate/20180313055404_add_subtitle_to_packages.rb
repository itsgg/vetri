class AddSubtitleToPackages < ActiveRecord::Migration[5.1]
  def change
    add_column :packages, :subtitle, :string
  end
end
