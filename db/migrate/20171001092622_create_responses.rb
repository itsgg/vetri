class CreateResponses < ActiveRecord::Migration[5.1]
  def change
    create_table :responses do |t|
      t.references :attempt, null: false
      t.references :question, null: false
      t.references :answer, null: false
      t.timestamps
    end
  end
end
