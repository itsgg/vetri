class AddRatingCountRatingValueToPackages < ActiveRecord::Migration[5.1]
  def change
    add_column :packages, :rating_count, :integer, default: 0
    add_column :packages, :rating_value, :integer, default: 0
  end
end
