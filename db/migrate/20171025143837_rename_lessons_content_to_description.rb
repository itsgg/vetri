class RenameLessonsContentToDescription < ActiveRecord::Migration[5.1]
  def change
    rename_column :lessons, :content, :description
  end
end
