class AddHasLessonToPackage < ActiveRecord::Migration[5.1]
  def change
    add_column :packages, :has_lesson, :boolean, null: false, default: true
  end
end
