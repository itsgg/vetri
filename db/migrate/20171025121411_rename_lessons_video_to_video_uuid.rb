class RenameLessonsVideoToVideoUuid < ActiveRecord::Migration[5.1]
  def change
    rename_column :lessons, :video, :video_uuid
  end
end
