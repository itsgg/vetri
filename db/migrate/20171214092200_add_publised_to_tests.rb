class AddPublisedToTests < ActiveRecord::Migration[5.1]
  def change
    add_column :tests, :published, :boolean, default: false
  end
end
