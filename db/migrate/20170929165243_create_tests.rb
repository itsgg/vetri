class CreateTests < ActiveRecord::Migration[5.1]
  def change
    create_table :tests do |t|
      t.string :title, null: false
      t.text :description, null: false
      t.references :package, null: false
      t.boolean :published, default: false
      t.timestamps
    end
  end
end
