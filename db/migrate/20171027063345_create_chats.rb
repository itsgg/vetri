class CreateChats < ActiveRecord::Migration[5.1]
  def change
    create_table :chats do |t|
      t.text :message, null: false
      t.references :user, null: false
      t.references :classroom, null: false
      t.timestamps
    end
  end
end
