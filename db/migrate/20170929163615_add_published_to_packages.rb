class AddPublishedToPackages < ActiveRecord::Migration[5.1]
  def change
    add_column :packages, :published, :boolean, default: false
  end
end
