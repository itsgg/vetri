class AddCostToPackages < ActiveRecord::Migration[5.1]
  def change
    add_column :packages, :cost, :decimal, null: false, default: 0
  end
end
