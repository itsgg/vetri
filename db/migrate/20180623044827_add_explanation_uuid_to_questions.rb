class AddExplanationUuidToQuestions < ActiveRecord::Migration[5.1]
  def change
    add_column :questions, :explanation_uuid, :string
  end
end
