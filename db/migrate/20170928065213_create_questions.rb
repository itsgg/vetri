class CreateQuestions < ActiveRecord::Migration[5.1]
  def change
    create_table :questions do |t|
      t.text :content, null: false
      t.references :answer
      t.timestamps
    end
  end
end
