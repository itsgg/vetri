class CreateAssigments < ActiveRecord::Migration[5.1]
  def change
    create_table :assigments do |t|
      t.references :test, null: false
      t.references :question, null: false
      t.timestamps
    end
  end
end
