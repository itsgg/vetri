class AddPresentationBaseUrlPresentationCountToClassrooms < ActiveRecord::Migration[5.1]
  def change
    add_column :classrooms, :presentation_baseurl, :string
    add_column :classrooms, :presentation_count, :integer, null: false, default: 0
  end
end
