class CreateClassrooms < ActiveRecord::Migration[5.1]
  def change
    create_table :classrooms do |t|
      t.string :name, null: false
      t.text :description
      t.datetime :start_time
      t.references :package, null: false
      t.timestamps
    end
  end
end
