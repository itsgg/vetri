class CreateAttempts < ActiveRecord::Migration[5.1]
  def change
    create_table :attempts do |t|
      t.references :user, null: false
      t.references :test, null: false
      t.integer :score
      t.timestamps
    end
  end
end

