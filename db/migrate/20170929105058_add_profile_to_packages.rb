class AddProfileToPackages < ActiveRecord::Migration[5.1]
  def change
    add_column :packages, :profile, :string
  end
end
