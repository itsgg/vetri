class AddHasTestToPackages < ActiveRecord::Migration[5.1]
  def change
    add_column :packages, :has_test, :boolean, null: false, default: true
  end
end
