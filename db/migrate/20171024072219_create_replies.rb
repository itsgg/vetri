class CreateReplies < ActiveRecord::Migration[5.1]
  def change
    create_table :replies do |t|
      t.text :content, null: false
      t.references :topic, null: false
      t.timestamps
    end
  end
end
