Rails.application.routes.draw do

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  mathjax 'mathjax'

  mount ActionCable.server, at: '/cable'

  require 'sidekiq/web'
  require 'admin_constraint'
  mount Sidekiq::Web => '/sidekiq', :constraints => AdminConstraint.new

  root 'packages#index'

  resources :packages do
    member do
      put 'enroll'
      put 'rate'
      get 'users'
    end

    resources :announcements
    resources :classrooms do
      member do
        put 'prepare_presentation'
        put 'initialize_live'
      end
      resources :chats
    end
    resources :topics do
      resources :replies
    end
    resources :lessons
    resources :tests do
      member do
        put 'start'
        get 'attend'
        get 'review'
        put 'select'
        put 'complete'
        get 'result'
        delete 'reattempt'
      end
    end
  end

  post '/images/upload', 'images#upload', defaults: { format: 'json' }

  get 'about', to: 'static#about'
  get 'privacy', to: 'static#privacy'
  get 'terms', to: 'static#terms'
  get 'blog', to: 'posts#index'

  namespace :api, defaults: { format: :json } do
    resources :users
    resources :packages
  end

  resources 'sessions', only: [:new, :create, :destroy]
  resources :users do
    member do
      put :shadow
    end
  end
  resources :posts

  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy'
  get 'register', to: 'users#new'
  post 'register', to: 'users#create'

  post 'start_payment', to: 'payments#start'
  post 'verify_payment', to: 'payments#verify'

  namespace :admin do
    root 'packages#index'
    resources :packages do
      member do
        put :enroll
        delete :unenroll
      end
      resources :announcements
      resources :classrooms
      resources :lessons do
        member do
          put 'update_video_uuid'
          delete 'delete_video'
        end
      end
      resources :tests do
        member do
          get 'questions'
          put 'add_question'
          put 'remove_question'
          get 'select_questions'
          get 'preview'
        end
      end
    end
    resources :questions do
      member do
        put 'update_explanation_uuid'
        delete 'destroy_explanation_uuid'
      end
    end
    resources :posts
    resources :users
  end
end
