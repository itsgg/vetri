# Introduction 

Akshi is educational platform that uses testing as a learning strategy.

# Setup

    brew install imagemagick
    brew install graphicsmagick
    brew install poppler
    brew install ghostscript
    brew install tesseract
    brew cask install wkhtmltopdf
    brew install redis

# Linux dependencies

    apt install graphicsmagick
    apt install python-poppler
    apt install tesseract-ocr
    apt install pdftk
    apt install libreoffice-core --no-install-recommends
    apt install redis-server

